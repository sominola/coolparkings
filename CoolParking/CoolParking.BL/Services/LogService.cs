﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logFilepath)
        {
            LogPath = logFilepath;
        }

        public void Write(string logInfo)
        {
            try
            {
                if (string.IsNullOrEmpty(LogPath))
                    throw new InvalidOperationException("The file path is empty or null!");
                using var streamWriter = new StreamWriter(LogPath, true, Encoding.Default);
                if (string.IsNullOrEmpty(logInfo))
                {
                    streamWriter.Write("");
                    return;
                }
                
                streamWriter.WriteLine(logInfo);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException("Log file not found");
            }

            return File.ReadAllText(LogPath);
        }
    }
}