﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.Json;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private List<TransactionInfo> _transactionList = new();

        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance(Settings.ParkingCapacity);
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += WithdrawVehicle;

            _logTimer = logTimer;
            _logTimer.Elapsed += LogTimerOnElapsed;

            _logService = logService;

            _withdrawTimer.Start();
            _logTimer.Start();
        }

        private void LogTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            if (!_transactionList.Any())
                _logService.Write("");
            foreach (var transaction in _transactionList)
            {
                var json = JsonSerializer.Serialize(transaction);
                _logService.Write(json);
            }

            _transactionList.Clear();
        }

        private void WithdrawVehicle(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.Vehicles)
            {
                var tariff = vehicle.VehicleType switch
                {
                    VehicleType.PassengerCar => Settings.TariffVehicle["PassengerCar"],
                    VehicleType.Truck => Settings.TariffVehicle["Truck"],
                    VehicleType.Bus => Settings.TariffVehicle["Bus"],
                    VehicleType.Motorcycle => Settings.TariffVehicle["Motorcycle"],
                    _ => throw new ArgumentOutOfRangeException()
                };
                if (vehicle.Balance <= 0)
                {
                    var withdraw = tariff * Settings.PenaltyRatio;
                    vehicle.Balance -= withdraw;
                    _parking.Balance += withdraw;
                    _transactionList.Add(new TransactionInfo(vehicle.Id, withdraw));
                    continue;
                }

                if (vehicle.Balance > 0 && vehicle.Balance < tariff)
                {
                    var difference = tariff - vehicle.Balance;
                    var withdraw = vehicle.Balance + difference * Settings.PenaltyRatio;
                    vehicle.Balance -= withdraw;
                    _parking.Balance += withdraw;
                    _transactionList.Add(new TransactionInfo(vehicle.Id, withdraw));
                    continue;
                }

                if (vehicle.Balance >= tariff)
                {
                    var withdraw = tariff;
                    vehicle.Balance -= withdraw;
                    _parking.Balance += withdraw;
                    _transactionList.Add(new TransactionInfo(vehicle.Id, withdraw));
                    continue;
                }
            }
        }

        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _withdrawTimer?.Dispose();
                _logTimer?.Dispose();
                Parking.Reset();
            }

            _disposed = true;
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Vehicles.Capacity - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var collection = _parking.Vehicles;
            collection ??= new List<Vehicle>();
            return collection.AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Capacity == _parking.Vehicles.Count)
                throw new InvalidOperationException("Parking is full");

            if (_parking.Vehicles.Any(v => v.Id == vehicle.Id))
            {
                throw new ArgumentException("Vehicle exists with this id");
            }

            _parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId.ToUpper());
            if (vehicle == null)
                throw new ArgumentException("Vehicle not found!");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException(
                    "The vehicle cannot be removed from the parking lot, because it has a negative balance!");
            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Sum cannot be less than 0");
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId.ToUpper());
            if (vehicle == null)
                throw new ArgumentException("Vehicle not found!");
            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionList.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }
    }
}