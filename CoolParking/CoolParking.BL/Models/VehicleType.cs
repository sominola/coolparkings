﻿// TODO: implement enum VehicleType.
//        Items: PassengerCar, Truck, Bus, Motorcycle.

namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        PassengerCar = 0,
        Truck = 1,
        Bus = 2,
        Motorcycle = 3
    }
}