﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        [JsonPropertyName("Id")]
        [Required, RegularExpression("(?i:[a-z]{2}-[0-9]{4}-[a-z]{2})", ErrorMessage = "Wrong Id"), StringLength(10, ErrorMessage = "Length id > 10")]
        public string VehicleId { get; }
        [JsonPropertyName("Sum")]
        [Required(ErrorMessage = "Specify Sum")]
        public decimal Sum { get; }

        public DateTime TimeTransaction { get; set; } = DateTime.Now;

        [JsonConstructor]
        public TransactionInfo(string vehicleId, decimal sum)
        {
            VehicleId = vehicleId;
            Sum = sum;
        }

        public override string ToString()
        {
            return $"{TimeTransaction}: {Sum} money withdrawn from vehicle with Id='{VehicleId}'.\n";
        }
    }
}