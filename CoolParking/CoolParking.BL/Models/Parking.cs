﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; }

        private static Parking _instance;

        private Parking(int capacity)
        {
            Vehicles = new List<Vehicle>(capacity);
            Balance = Settings.StartBalanceParking;
        }

        public static Parking GetInstance(int capacity)
        {
            return _instance ??= new Parking(capacity);
        }

        public static void Reset()
        {
            _instance = null;
        }
    }
}