﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly Dictionary<string, decimal> TariffVehicle;

        static Settings()
        {
            TariffVehicle = new Dictionary<string, decimal>();
            TariffVehicle.Add("PassengerCar", 2);
            TariffVehicle.Add("Truck", 5);
            TariffVehicle.Add("Bus", 3.5m);
            TariffVehicle.Add("Motorcycle", 1);
        }

        public const decimal StartBalanceParking = 0;
        public const int ParkingCapacity = 10;
        public const double PeriodDebit = 5000;

        public const double PeriodLogger = 60000;

        //
        // public const decimal TariffPassengerCar = 2;
        // public const decimal TariffTruck = 5;
        // public const decimal TariffBus = 3.5m;
        // public const decimal TariffMotorcycle = 1;
        //
        public const decimal PenaltyRatio = 2.5m;
    }
}