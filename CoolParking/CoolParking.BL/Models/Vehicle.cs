﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonPropertyName("Id")]
        public string Id { get; init; }

        [JsonPropertyName("VehicleType")]
        public VehicleType VehicleType { get; init; }
        
        [JsonPropertyName("Balance")]
        public decimal Balance { get; internal set; }
        
        [JsonConstructor]
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            
            if (balance < 0)
                throw new ArgumentException("Balance cannot be < than 0");
            if ((int)vehicleType < 0 || (int)vehicleType > 3)
                throw new ArgumentException("Wrong type car");
            const string pattern = @"(?i:[a-z]{2}-[0-9]{4}-[a-z]{2})";
            if (!Regex.IsMatch(id, pattern) || id.Length != 10)
                throw new ArgumentException("Wrong id vehicle");

            Id = id.ToUpper();
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var number = new StringBuilder();
            var random = new Random();
            for (int i = 0; i < 2; i++)
            {
                int num = random.Next(0, 26);
                char let = (char) ('a' + num);
                number.Append(let);
            }

            number.Append('-');
            for (int i = 0; i < 4; i++)
            {
                number.Append(random.Next(10));
            }

            number.Append('-');
            for (int i = 0; i < 2; i++)
            {
                int num = random.Next(0, 26);
                char let = (char) ('a' + num);
                number.Append(let);
            }

            return number.ToString().ToUpper();
        }
    }
}