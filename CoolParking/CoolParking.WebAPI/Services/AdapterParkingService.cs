﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.Json;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class AdapterParkingService : IAdapterParkingService
    {
        private readonly IParkingService _service;

        public AdapterParkingService(IParkingService service)
        {
            _service = service;
        }

        public void Dispose()
        {
            _service?.Dispose();
        }

        public decimal GetBalance() => _service.GetBalance();


        public int GetCapacity() => _service.GetCapacity();

        public int GetFreePlaces() => _service.GetFreePlaces();

        public ReadOnlyCollection<Vehicle> GetVehicles() => _service.GetVehicles();

        public void AddVehicle(Vehicle vehicle) => _service.AddVehicle(vehicle);

        public void RemoveVehicle(string vehicleId) => _service.RemoveVehicle(vehicleId);

        public Vehicle TopUpVehicle(string id, decimal sum)
        {
            _service.TopUpVehicle(id, sum);
            return GetVehicle(id);
        }

        void IParkingService.TopUpVehicle(string vehicleId, decimal sum) => _service.TopUpVehicle(vehicleId, sum);


        public TransactionInfo[] GetLastParkingTransactions() => _service.GetLastParkingTransactions();

        string IParkingService.ReadFromLog() => _service.ReadFromLog();


        public string ReadFromLog()
        {
            var data = _service.ReadFromLog();
            var lineArray = data.Split('\n');
            var listTransaction = "";
            foreach (var json in lineArray)
            {
                if (string.IsNullOrEmpty(json)) continue;
                var transaction = JsonSerializer.Deserialize<TransactionInfo>(json);

                if (transaction == null)
                {
                    throw new NullReferenceException();
                }

                listTransaction += transaction + "\n";
            }

            return listTransaction;
        }

        public Vehicle GetVehicle(string id)
        {
            var listVehicles = _service.GetVehicles();
            return listVehicles.FirstOrDefault(vehicle => vehicle.Id == id.ToUpper());
        }
    }
}