using System;
using System.IO;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new FormatConverter());
            });

            services.AddSingleton<IAdapterParkingService, AdapterParkingService>(servicesProvider =>
            {
                var parkingService = new ParkingService
                (
                    new TimerService(Settings.PeriodDebit),
                    new TimerService(Settings.PeriodLogger),
                    new LogService(Directory.GetCurrentDirectory() + @"\Transactions.log")
                );
                return new AdapterParkingService(parkingService);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseExceptionHandler(c => c.Run(async context =>
            {
                var exception = context.Features
                    .Get<IExceptionHandlerPathFeature>()
                    .Error;
                var response = new {error = exception.Message};
                await context.Response.WriteAsJsonAsync(response);
            }));

            app.Use(async (context, next) =>
            {
                Console.WriteLine($"Started handling request!");
                await next.Invoke();
                Console.WriteLine("Finished handling request!");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}