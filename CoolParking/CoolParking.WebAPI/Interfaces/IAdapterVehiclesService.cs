﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IAdapterParkingService : IParkingService
    {
        new string ReadFromLog();
        new Vehicle TopUpVehicle(string id, decimal sum);
        Vehicle GetVehicle(string id);
    }
}