﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI
{
    public class FormatConverter : JsonConverter<decimal>
    {
        public override decimal Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            switch (reader.TokenType)
            {
                case JsonTokenType.String:
                    string stringValue = reader.GetString();
                    if (decimal.TryParse(stringValue, out decimal value))
                    {
                        throw new JsonException();
                    }

                    break;
            }

            return reader.GetDecimal();
        }

        public override void Write(Utf8JsonWriter writer, decimal value, JsonSerializerOptions options)
        {
            writer.WriteNumberValue(value);
        }
    }
}