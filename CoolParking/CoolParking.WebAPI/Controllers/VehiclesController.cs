﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly IAdapterParkingService _service;

        public VehiclesController(IAdapterParkingService service) => _service = service;


        [HttpGet]
        public ActionResult<List<Vehicle>> Get()
        {
            return Ok(_service.GetVehicles());
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            if (!Regex.IsMatch(id, @"(?i:[a-z]{2}-[0-9]{4}-[a-z]{2})") || id.Length != 10)
                return BadRequest("Id is invalid");

            var vehicle = _service.GetVehicle(id);

            if (vehicle == null)
                return NotFound("Vehicle not found");

            return Ok(vehicle);
        }

        [HttpPost]
        public ActionResult<Vehicle> Create([FromBody] object vehicle)
        {
            try
            {
                var data = vehicle.ToString();
                var car = JsonSerializer.Deserialize<Vehicle>(data);
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                _service.AddVehicle(car);
                return Created("api/vehicles", car);
            }
            catch (JsonException)
            {
                return BadRequest();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            if (!Regex.IsMatch(id, @"(?i:[a-z]{2}-[0-9]{4}-[a-z]{2})") || id.Length != 10)
                return BadRequest("Id is invalid");
            try
            {
                _service.RemoveVehicle(id.ToUpper());
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            return NoContent();
        }
    }
}