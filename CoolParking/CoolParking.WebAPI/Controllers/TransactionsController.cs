﻿using System;
using System.Collections.Generic;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly IAdapterParkingService _service;
        public TransactionsController(IAdapterParkingService service) => _service = service;

        [HttpGet("last")]
        public ActionResult<List<TransactionInfo>> GetLastTransactions()
        {
            return Ok(_service.GetLastParkingTransactions());
        }

        [HttpGet("all")]
        public ActionResult<string> GetTransactionFromLog()
        {
            try
            {
                var result = _service.ReadFromLog();
                if (string.IsNullOrEmpty(result))
                    throw new InvalidOperationException("Log file not found");
                
                return Ok(_service.ReadFromLog());
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle(TransactionInfo transaction)
        {
            if (transaction.Sum < 0)
                return BadRequest("Sum cannot be negative!");
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var vehicle = _service.TopUpVehicle(transaction.VehicleId, transaction.Sum);
                return Ok(vehicle);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}