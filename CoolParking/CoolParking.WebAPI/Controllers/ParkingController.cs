﻿using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private readonly IAdapterParkingService _service;

        public ParkingController(IAdapterParkingService service) => _service = service;

        [HttpGet("balance")]
        public ActionResult GetBalance()
        {
            return Ok(_service.GetBalance());
        }

        [HttpGet("capacity")]
        public ActionResult GetCapacity()
        {
            return Ok(_service.GetCapacity());
        }

        [HttpGet("freePlaces")]
        public ActionResult GetFreePlaces()
        {
            return Ok(_service.GetFreePlaces());
        }
    }
}