﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Text.Json;
using CoolParking.BL.Models;


namespace CoolParking.Interface
{
    class Program
    {
        private static RequestHandler _request;

        static void Main(string[] args)
        {
            _request = new RequestHandler();
            Menu();
        }

        public static void Menu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. Display the current balance of the Parking.");
                Console.WriteLine(
                    "2. Display the amount of earned funds for the current period (before recording in the log).");
                Console.WriteLine("3. Display the number of free parking spaces (free X with Y).");
                Console.WriteLine("4. Display all Parking Transactions for the current period (before logging).");
                Console.WriteLine("5. Display the transaction history (reading data from the Transactions.log file).");
                Console.WriteLine("6. Display a list of vehicles in the parking lot.");
                Console.WriteLine("7. Put the vehicle in the parking lot.");
                Console.WriteLine("8. Pick up the Vehicle from the Parking.");
                Console.WriteLine("9. Top up the balance of a specific Vehicle.");
                Console.Write("Enter the action number: ");
                var input = Console.ReadLine();
                int choice;
                int.TryParse(input, out choice);
                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        DisplayBalanceParking();
                        Console.ReadLine();
                        break;
                    case 2:
                        Console.Clear();
                        DisplayAmountEarned();
                        Console.ReadLine();

                        break;
                    case 3:
                        Console.Clear();
                        DisplayFreePlaces();
                        Console.ReadLine();

                        break;
                    case 4:
                        Console.Clear();
                        DisplayLastTransactions();
                        Console.ReadLine();

                        break;
                    case 5:
                        Console.Clear();
                        DisplayTransactionsFromFile();
                        Console.ReadLine();
                        break;
                    case 6:
                        Console.Clear();
                        DisplayListVehicles();
                        Console.ReadLine();

                        break;
                    case 7:
                        Console.Clear();
                        PutVehicleInParking();
                        Console.ReadLine();

                        break;
                    case 8:
                        Console.Clear();
                        PickUpVehicleFromParking();
                        Console.ReadLine();

                        break;
                    case 9:
                        Console.Clear();
                        TopUpBalanceVehicle();
                        Console.ReadLine();
                        break;
                    default:
                        Console.Clear();

                        break;
                }
            }
        }

        private static void DisplayBalanceParking()
        {
            var response = _request.GetStringAsync("parking/balance").Result;
            decimal balance;
            decimal.TryParse(response, out balance);
            Console.WriteLine("Current parking balance: " + balance);
        }

        private static void DisplayAmountEarned()
        {
            var response = _request.GetResponseAsync("transactions/last").Result;
            var content = response.Content.ReadAsStringAsync().Result;
            var lastTransactions = JsonSerializer.Deserialize<List<TransactionInfo>>(content);
            decimal sum = lastTransactions.Sum(transaction => transaction.Sum);
            Console.WriteLine(
                $"The amount of earned funds for the current period (before recording in the log): " + sum);
        }

        private static void DisplayFreePlaces()
        {
            var firstResponse = _request.GetStringAsync("parking/freePlaces").Result;
            int freePlaces;
            int.TryParse(firstResponse, out freePlaces);
            var secondResponse = _request.GetStringAsync("parking/capacity").Result;
            int capacity;
            int.TryParse(secondResponse, out capacity);
            Console.WriteLine("Number of available parking spaces: " + freePlaces + " out of " + capacity);
        }

        private static void DisplayLastTransactions()
        {
            var response = _request.GetStringAsync("transactions/last").Result;
            var listTransactions = JsonSerializer.Deserialize<List<TransactionInfo>>(response);
            if (!listTransactions.Any())
            {
                Console.WriteLine("List transactions empty!");
            }

            foreach (var transaction in listTransactions)
            {
                Console.WriteLine($"Transaction sum: {transaction.Sum}");
                Console.WriteLine($"Transaction time: {transaction.TimeTransaction}");
                Console.WriteLine($"Transaction vehicle Id: {transaction.VehicleId}");
                Console.WriteLine();
            }
        }

        private static void DisplayTransactionsFromFile()
        {
            var response = _request.GetResponseAsync("transactions/all").Result;
            switch (response.StatusCode)
            {
                case HttpStatusCode.NotFound:
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    return;
                case HttpStatusCode.OK:
                    var message = response.Content.ReadAsStringAsync().Result;
                    var lineArray = message.Split('\n');

                    foreach (var line in lineArray)
                    {
                        Console.WriteLine(line);
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static void DisplayListVehicles()
        {
            var response = _request.GetStringAsync("vehicles").Result;
            var listVehicles = JsonSerializer.Deserialize<List<Vehicle>>(response);
            if (!listVehicles.Any())
            {
                Console.WriteLine("List vehicles empty!");
            }

            foreach (var vehicle in listVehicles)
            {
                Console.WriteLine($"Vehicle type: {vehicle.VehicleType.ToString()}");
                Console.WriteLine($"Vehicle balance: {vehicle.Balance}");
                Console.WriteLine($"Vehicle Id: {vehicle.Id}");
                Console.WriteLine();
            }
        }

        private static void PutVehicleInParking()
        {
            var isSelectedType = false;
            var vehicleType = VehicleType.PassengerCar;
            while (!isSelectedType)
            {
                Console.Clear();
                Console.WriteLine("Select the vehicle type: ");
                Console.WriteLine("1 - Passenger Car");
                Console.WriteLine("2 - Truck");
                Console.WriteLine("3 - Bus");
                Console.WriteLine("4 - Motorcycle");
                Console.WriteLine("1001 - Exit to main menu");
                Console.Write("Choose vehicle: ");
                var line = Console.ReadLine();
                int choiceType;
                int.TryParse(line, out choiceType);
                switch (choiceType)
                {
                    case 1:
                        vehicleType = VehicleType.PassengerCar;
                        isSelectedType = true;
                        break;
                    case 2:
                        vehicleType = VehicleType.Truck;
                        isSelectedType = true;

                        break;
                    case 3:
                        vehicleType = VehicleType.Bus;
                        isSelectedType = true;

                        break;
                    case 4:
                        vehicleType = VehicleType.Motorcycle;
                        isSelectedType = true;

                        break;
                    case 1001:
                        return;
                    default:
                        continue;
                }
            }

            Console.Write("Enter the vehicle number (XX-YYYY-XX): ");
            var idVehicle = Console.ReadLine();

            Console.Write("Enter your vehicle balance: ");
            decimal balance;
            decimal.TryParse(Console.ReadLine(), out balance);
            try
            {
                var vehicle = new Vehicle(idVehicle, vehicleType, balance);
                var response = _request.PostRequestAsync("vehicles", vehicle).Result;
                switch (response.StatusCode)
                {
                    case HttpStatusCode.BadRequest:
                        var message = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(message);
                        return;

                    case HttpStatusCode.Created:
                        var messageVehicle = response.Content.ReadFromJsonAsync<Vehicle>().Result;
                        Console.WriteLine("The vehicle was successfully created");
                        Console.WriteLine("Vehicle id: " + messageVehicle.Id);
                        Console.WriteLine("Vehicle balance: " + messageVehicle.Balance);
                        Console.WriteLine("Vehicle Type: " + messageVehicle.VehicleType.ToString());
                        break;
                }
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            
        }

        private static void PickUpVehicleFromParking()
        {
            Console.Write("Enter the vehicle id: ");
            var input = Console.ReadLine();
            var response = _request.DeleteRequestAsync("vehicles/" + input).Result;
            switch (response.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    var message = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(message);
                    break;
                case HttpStatusCode.NotFound:
                    Console.WriteLine("Vehicle not found!");
                    break;
                case HttpStatusCode.NoContent:
                    Console.WriteLine("Vehicle successfully removed!");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static void TopUpBalanceVehicle()
        {
            Console.Write("Enter vehicle id: ");
            var id = Console.ReadLine();
            Console.Write("Enter sum: ");
            var inputSum = Console.ReadLine();
            decimal sum;
            decimal.TryParse(inputSum, out sum);
            var transaction = new TransactionInfo(id, sum);
            var response = _request.PutRequestAsync("transactions/topUpVehicle", transaction).Result;
            switch (response.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    var firstMessage = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(firstMessage);
                    break;
                case HttpStatusCode.NotFound:
                    var secondMessage = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(secondMessage);
                    break;
                case HttpStatusCode.OK:
                    var thirdMessage = response.Content.ReadAsStringAsync().Result;
                    var vehicle = JsonSerializer.Deserialize<Vehicle>(thirdMessage);
                    Console.WriteLine("Balance successfully top up");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}

