using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CoolParking.Interface
{
    public class RequestHandler : IDisposable
    {
        private readonly HttpClient _client;
        private readonly HttpClientHandler _clientHandler;
        private bool _disposed;

        public RequestHandler(IWebProxy proxy = null)
        {
            _clientHandler = new HttpClientHandler {UseProxy = proxy != null, Proxy = proxy};
            _client = new HttpClient(_clientHandler)
            {
                BaseAddress = new Uri("https://localhost:5001/api/")
            };
        }

        public async Task<string> GetStringAsync(string url)
        {
            return await _client.GetStringAsync(url);
        }

        public async Task<HttpResponseMessage> GetResponseAsync(string url)
        {
            return await _client.GetAsync(url);
        }

        public async Task<HttpResponseMessage> DeleteRequestAsync(string url)
        {
            return await _client.DeleteAsync(url);
        }

        public async Task<HttpResponseMessage> PostRequestAsync<T>(string url, T content)
        {
            return await _client.PostAsJsonAsync(url, content);
        }

        public async Task<HttpResponseMessage> PutRequestAsync<T>(string url, T content)
        {
            return await _client.PutAsJsonAsync(url, content);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _clientHandler.Dispose();
                _client.Dispose();
            }

            _disposed = true;
        }
    }
}